# Libreoffice Draw Atomicity Template

The libreoffice draw atomicity template is a ready made and well designed flowchart useful for comparing objects using pros and cons with the structure of an atom involving revolving electrons as factors for discussions. It is improving as quickly as possible. Download this libreoffice draw template from https://extensions.libreoffice.org/en/extensions/show/20586.

# Atomicity pre-geometry (Second version of template)

# Fixes :-
- Fixed geometric positions (Aligned the white dot in centre, electron positions yet to be fixed)
- Renamed pages to respective codenames (i.e., renamed to specific shape names)

# Features :-
- Configured new arms (Added new arms)

If you have any ideas to decorate or fix some features for this template, then please submit it through the Issues section
